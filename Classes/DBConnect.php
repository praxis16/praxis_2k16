<?php
    class DBConnect{
        public $servername = "";
        public $username = "";
        public $password = "";
        public $dbName = "";
        public $connect = "";
        public function __construct(){
            $this->servername = "localhost";
            $this->username = "root";
            $this->password = "";
            $this->dbName="praxis16";
            $this->connect = mysqli_connect($this->servername, $this->username, $this->password, $this->dbName);
            if (!$this->connect) {
                die("Connection failed: " . mysqli_connect_error());
            }
        }
    }
?>