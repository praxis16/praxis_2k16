<!DOCTYPE html>
<html lang="en">
<head>
    <title>Praxis 2K16 </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="../assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="../assets/css/ie8.css" /><![endif]-->
</head>
<body class="landing">
    <div id="page-wrapper">

        <header id="header">
                    <h1 id="logo"><a href="../home.php">Praxis</a></h1>
                    <nav id="nav">
                        <ul>
                            <li><a href="home.php">Home</a></li>
                            <li><a href="events.php">Events</a></li>
                            <li><a href="sponsors.php">Sponsors</a></li>
                            <li><a href="schedule.php">Schedule</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </nav>
        </header>

        <section id="banner">
            <div class="content">
                <header>
                    <h2>V.E.S. Institute of Technology</h2>
                </header>
                <!--Add VES logo here-->
                <span class="image"><img src="../images/pic01.jpg" alt="" /></span>
            </div>
            <a href="#one" class="goto-next scrolly">Next</a>
        </section>

        <section id="one" class="spotlight style1 bottom">
        <!--Add VES building photo here-->
        					<span class="image  main"><img src="../images/VESIT.jpg" alt="" /></span>
        					<div class="content">
        						<div class="container">
        							<div class="row">
        								<div class="4u 12u$(medium)">
        									<header>
        										<h2>Vision</h2>
        										<p>To create a vibrant knowledge-oriented environment with innovative teaching practices and to inculcate a tradition of socially conscious application of technology</p>
        									</header>
        								</div>
        								<div class="4u 12u$(medium)">
        									<p>Vivekanand Education Society's Institute of Technology also known as VESIT
        									 or V.E.S. Institute of Technology, established in 1984, is an engineering college affiliated
        									  with the University of Mumbai.</p>
        								</div>
        								<div class="4u$ 12u$(medium)">
        									<p>The campus is located in Chembur, a northeastern suburb of Mumbai.
        									VESIT shares its campus with the college of Pharmacy and the college for Management and Research.
        									 The campus comprises one big C-shaped building for the engineering college and several other buildings
        									  for the other colleges, hostels, etc</p>
        								</div>
        							</div>
        						</div>
        					</div>
        					<a href="#two" class="goto-next scrolly">Next</a>
        </section>
        <section id="two" class="spotlight style1 bottom">
                					<div class="content">
                						<div class="container">
                							<div class="row">
                								<div class="4u 12u$(medium)">
                									<header class="major">
                										<h2>People of VESIT</h2>
                									</header>
                								</div>
                								<div class="4u 12u$(medium)">
                									<p><span class="image left"><img src="../images/principal.jpg" alt="" /></span><b>Dr.(Mrs.) J.M.Nair</b><br/> Principal, VESIT</p>
                								</div>
                								<div class="4u$ 12u$(medium)">
                									 <p><span class="image left"><img src="../images/vice-principal.jpg" alt="" /></span><b>Mrs.Vijayalakxmi M.</b><br/>Vice-Principal, VESIT</p>
                								</div>
                								<div class="4u$ 12u$(medium)">
                                                      <p><span class="image left"><img src="../images/sabnis_sir.jpg" alt="" /></span><b>Prof. Manoj Sabnis</b><br/>Dean of Student Affairs, VESIT<br/></p>
                                                 </div>
                							</div>
                						</div>
                					</div>
                </section>

        <footer id="footer">
            <ul class="icons">
                <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon alt fa-snapchat"><span class="label">Snapchat</span></a></li>
                <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <ul class="copyright">
                <li>&copy;  All rights reserved.</li><li>Design: <a href="http://html5up.net"> Praxis Technical Team</a></li>
            </ul>
        </footer>

    </div>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/jquery.scrolly.min.js"></script>
        <script src="../assets/js/jquery.dropotron.min.js"></script>
        <script src="../assets/js/jquery.scrollex.min.js"></script>
        <script src="../assets/js/skel.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="../assets/js/main.js"></script>
</body>
</html>