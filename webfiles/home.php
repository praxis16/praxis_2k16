<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18-07-2016
 * Time: 14:51
 */
    require(__DIR__."/../Classes/ApiMiddleware.php");
    $api = new ApiMiddleware();

    $request = array(
        'url' => 'sponsors',
        'method' => 'GET',
        'body' => NULL
    );

    //foreach($responses as $response){
     //   echo $response ;
    //}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Praxis 2K16 </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="../assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="../assets/css/ie8.css" /><![endif]-->
</head>
<body class="landing">
    <div id="page-wrapper">

        <header id="header">
            <h1 id="logo"><a href="../home.php">Praxis</a></h1>
            <nav id="nav">
                <ul>
                    <li><a href="home.php">Home</a></li>
                    <li><a href="events.php">Events</a></li>
                    <li><a href="sponsors.php">Sponsors</a></li>
                    <li><a href="schedule.php">Schedule</a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="contact-us.php">Contact Us</a></li>
                </ul>
            </nav>
        </header>

        <section id="banner">
            <div class="content">
                <header>
                    <h2>Praxis 2016</h2>
                    <p>From theory to Practise<br />
                    </p>
                </header>
                <!--Add Praxis Image here-->
                <span class="image"><img src="../images/pic01.jpg" alt="" /></span>
            </div>
            <a href="#one" class="goto-next scrolly">Next</a>
        </section>

        <section id="one" class="spotlight style1 bottom">
            <!--Praxis Amphi Image-->
            <span class="image fit main"><img src="../images/pic02.jpg" alt="" /></span>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="4u 12u$(medium)">
                            <header>
                                <h2>Praxis 2k16</h2>
                                <h2>Schedule</h2>
                            </header>
                        </div>
                        <div class="4u 12u$(medium)">
                            <header>
                                <h2>September 23,24</h2>
                                <h2>Online treasure hunt</h2>
                            </header>
                        </div>
                        <div class="4u$ 12u$(medium)">
                            <header>
                                <h2>Odio faucibus ipsum integer consequat</h2>
                                <h2>Odio faucibus ipsum integer consequat</h2>
                            </header>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#two" class="goto-next scrolly">Next</a>
        </section>

        <section id="four" class="wrapper style1 special fade-up">
            <div class="container">
                <header class="major">
                    <h2>Sponsors</h2>
                </header>
                <div class="box alt">
                    <div class="row uniform">
                        <section class="4u 6u(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-area-chart"></span>
                            <h3>Google</h3>
                        </section>
                        <section class="4u 6u$(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-comment"></span>
                            <h3>Tesla</h3>
                        </section>
                        <section class="4u$ 6u(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-flask"></span>
                            <h3>SpaceX</h3>
                        </section>
                        <section class="4u 6u$(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-paper-plane"></span>
                            <h3>Facebook</h3>
                        </section>
                        <section class="4u 6u(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-file"></span>
                            <h3>Alphabet</h3>
                        </section>
                        <section class="4u$ 6u$(medium) 12u$(xsmall)">
                            <span class="icon alt major fa-lock"></span>
                            <h3>Momo</h3>
                        </section>
                    </div>
                </div>
                <footer class="major">
                    <ul class="actions">
                        <li><a href="sponsors.php" class="button">Learn More</a></li>
                    </ul>
                </footer>
            </div>
        </section>

        <footer id="footer">
            <ul class="icons">
                <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon alt fa-snapchat"><span class="label">Snapchat</span></a></li>
                <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <ul class="copyright">
                <li>&copy;  All rights reserved.</li><li>Design: <a href="http://html5up.net"> Praxis Technical Team</a></li>
            </ul>
        </footer>

    </div>

        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/jquery.scrolly.min.js"></script>
        <script src="../assets/js/jquery.dropotron.min.js"></script>
        <script src="../assets/js/jquery.scrollex.min.js"></script>
        <script src="../assets/js/skel.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="../assets/js/main.js"></script>
</body>
</html>