<?php
function test_data($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
$err_msg = $suc_msg = '';
if($_SERVER["REQUEST_METHOD"] == "POST") {
    require(__DIR__."/../Classes/ApiMiddleware.php");
    $api = new ApiMiddleware();
    $error = false;
    if(isset($_POST['name'])) {
        $name = test_data($_POST['name']);
        if($name == '') {
            $error = true;
            $err_msg = 'Name cannot be empty';
        } else {
            $error = $error === true;
        }
    }

    if(isset($_POST['email'])) {
        $email = test_data($_POST['email']);
        if($email == '') {
            $error = true;
            $err_msg = 'Email ID cannot be empty';
        }
        else {
            $error = $error === true;
        }
    }

    if(isset($_POST['message'])) {
        $description = test_data($_POST['message']);
        if($description == '') {
            $error = true;
            $err_msg = 'There was an error, fill in apt. description.';
        }else {
            $error = $error === true;
        }
    }

    if(!$error) {
        $body = array(
            'name'        => $name,
            'email'       => $email,
            'description' => $description
        );

        $req = array(
            'url'    => 'contact_us',
            'method' => 'POST',
            'body'   => $body
        );
        $res = $api->initRequest($req);
        if($res['http_code'] == 200 || $res['http_code'] == 201) {
            $suc_msg = 'Thankyou for contacting us, we will revert soon';
        }
    }

}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="../assets/css/main.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="../assets/css/ie9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="../assets/css/ie8.css" /><![endif]-->
        <script language="javascript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <style>
            #map {
                height: 100%;
            }
        </style>
    </head>
    <body class="landing">
        <div id="page-wrapper">

            <header id="header">
                <h1 id="logo"><a href="../home.php">Praxis</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="home.php">Home</a></li>
                        <li><a href="events.php">Events</a></li>
                        <li><a href="sponsors.php">Sponsors</a></li>
                        <li><a href="schedule.php">Schedule</a></li>
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                    </ul>
                </nav>
            </header>

            <section id="banner">
                <div class="content">
                    <header>
                        <h2>Contact Us</h2>
                    </header>
                </div>
                <a href="#main" class="goto-next scrolly">Next</a>
            </section>

            <div id="main" class="wrapper style1">
                <div class="container">
                    <!--<header class="major">
                        <h2>Contact Us</h2>
                    </header>-->
                    <section>
                        <div id="map"></div>
                        <script>

                            function initMap() {
                                var myLatLng = {lat: 19.046116, lng: 72.889813};

                                var map = new google.maps.Map(document.getElementById('map'), {
                                    zoom: 4,
                                    center: myLatLng
                                });

                                var marker = new google.maps.Marker({
                                    position: myLatLng,
                                    map: map,
                                    title: 'VESIT!'
                                });
                            }
                        </script>
                        <script async defer
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxgKWaeFSteJv9Rgejc24-QW_YkGqupRw&callback=initMap">
                        </script>
                    </section>
                    <br><br><br><br>
                    <section>
                        <?php
                        if($suc_msg !== '') {
                        ?><center><p style="color: green; font-weight: bolder"><?=$suc_msg?></p></center>
                        <?php
                        } if($err_msg !== '') {?>
                        <center><p style="color:red;"><?=$err_msg?></p></center>
                        <?php }?>
                        <form method="post" action="contact-us.php">
                            <div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="name" id="name" value="" placeholder="Name" required/>
                                </div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="email" name="email" id="email" value="" placeholder="Email" required/>
                                </div>
                                <div class="12u$">
                                    <textarea name="message" id="message" placeholder="Enter your message" rows="6" required></textarea>
                                </div>
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Send Message" class="special" /></li>
                                        <li><input type="reset" value="Reset" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </section>
                    <section>
                        <header>
                            <h3>Contact Info</h3>

                            <i class="fa fa-map-marker fa-6" aria-hidden="true"></i>
                            <h6>Address</h6>
                            <p>V.E.S.I.T ,Hashu Advani Memorial Complex, Collector Colony, Chembur East, Mumbai, Maharashtra 400074</p>
                        </header>
                    </section>
                </div>
            </div>
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-snapchat"><span class="label">Snapchat</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy;  All rights reserved.</li><li>Design: <a href="http://html5up.net"> Praxis Technical Team</a></li>
                </ul>
            </footer>
        </div>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/jquery.scrolly.min.js"></script>
        <script src="../assets/js/jquery.dropotron.min.js"></script>
        <script src="../assets/js/jquery.scrollex.min.js"></script>
        <script src="../assets/js/skel.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="../assets/js/main.js"></script>
    </body>
</html>
