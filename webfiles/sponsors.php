<?php

require(__DIR__."/../Classes/ApiMiddleware.php");
$api = new ApiMiddleware();

$request = array(
    'url' => 'sponsors',
    'method' => 'GET',
    'body' => NULL
);
$responses = $api->initRequest($request);
//print_r($responses[data]);
$r=$responses['data'];
$s=json_encode($r);
?>
<html lang="en">
    <head>
	    	<title>Sponsors Praxis 2K16</title>
		    <meta charset="utf-8" />
	    	<meta name="viewport" content="width=device-width, initial-scale=1" />
		    <!--[if lte IE 8]><script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
		    <link rel="stylesheet" href="../assets/css/main.css" />
		    <!--[if lte IE 9]><link rel="stylesheet" href="../assets/css/ie9.css" /><![endif]-->
		    <!--[if lte IE 8]><link rel="stylesheet" href="../assets/css/ie8.css" /><![endif]-->
    </head>
    <body class="landing">
        <div id="page-wrapper">

            <header id="header">
                                <h1 id="logo"><a href="../home.php">Praxis</a></h1>
                                <nav id="nav">
                                    <ul>
                                        <li><a href="home.php">Home</a></li>
                                        <li><a href="events.php">Events</a></li>
                                        <li><a href="sponsors.php">Sponsors</a></li>
                                        <li><a href="schedule.php">Schedule</a></li>
                                        <li><a href="about.php">About Us</a></li>
                                        <li><a href="contact-us.php">Contact Us</a></li>
                                    </ul>
                                </nav>
            </header>

            <section id="banner">
                        <div class="content">
                            <header>
                                <h2>Sponsors</h2>
                            </header>
                        </div>
                        <a href="#0" class="goto-next scrolly">Next</a>
                    </section>
            <!--
            Following script was made to try access of info from api
            <script>
            var arr = <?php echo $s ?>;
            console.log(arr);
            //echo JSON.parse(arr);
                var i;
                document.write("<table>");

                for(i = 0; i < arr.length; i++) {
                    document.write( "<tr><td>" +
                    arr[i].event_name +
                    "</td><td>" +
                    arr[i].category +
                    "</td><td>" +
                    arr[i].description +
                    "</td><td>" +
                    "teams of" +arr[i].teams_of +
                    "</td><td>" +
                    "prize"+arr[i].prize +
                    "</td><td>" +
                    "date"+arr[i].date +
                    "</td></tr>");
                }
                document.write("</table>");
            </script>
            -->

        	<script>
        	    //dummy test
        	   //var text ='[' +
                         '{ "image":"www.google.com" , "tagline":"Doe" , "description":"radio" , "link":"www.google.com"},' +
                         '{ "image":"www.google.com" , "tagline":"Smith" , "description":"ga" , "link":"www.google.com"},' +
                         '{ "image":"www.google.com" , "tagline":"Jones" , "description":"ga" , "link":"www.google.com"} ]';
               //var arrr = JSON.parse(text);
               //actual access
               var arrr = <?php echo $s ?>;
               console.log(arrr);
               for(i = 0; i < arrr.length-1; i++) {
                if(i%2==0){
                    document.write(
                         '<section id="' + i +'" class="spotlight style2 right"><span class="image fit"><img src="' + arrr[i].image + '" alt="" /></span><div class="content"><header><p>' + arrr[i].tagline + '</p></header><p>' + arrr[i].description + '</p><ul class="actions"><li><a href="' + arrr[i].link + '" class="button">Learn More</a></li></ul></div><a href="#' + (i+1) +'" class="goto-next scrolly">Next</a></section>'
                    );
                }
                else{
                    document.write(
                         '<section id="' + i +'" class="spotlight style3 left"><span class="image fit"><img src="' + arrr[i].image + '" alt="" /></span><div class="content"><header><p>' + arrr[i].tagline + '</p></header><p>' + arrr[i].description + '</p><ul class="actions"><li><a href="' + arrr[i].link + '" class="button">Learn More</a></li></ul></div><a href="#' + (i+1) +'" class="goto-next scrolly">Next</a></section>'
                    );
                }
                }
                if(i==arrr.length-1){
                if(i%2==0){
                                    document.write(
                                         '<section id="' + i +'" class="spotlight style2 right"><span class="image fit"><img src="' + arrr[i].image + '" alt="" /></span><div class="content"><header><p>' + arrr[i].tagline + '</p></header><p>' + arrr[i].description + '</p><ul class="actions"><li><a href="' + arrr[i].link + '" class="button">Learn More</a></li></ul></div></section>'
                                    );
                                }
                                else{
                                    document.write(
                                         '<section id="' + i +'" class="spotlight style3 left"><span class="image fit"><img src="' + arrr[i].image + '" alt="" /></span><div class="content"><header><p>' + arrr[i].tagline + '</p></header><p>' + arrr[i].description + '</p><ul class="actions"><li><a href="' + arrr[i].link + '" class="button">Learn More</a></li></ul></div></section>'
                                    );
                                }
                    }

        	</script>
        	<footer id="footer">
                        <ul class="icons">
                            <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="#" class="icon alt fa-snapchat"><span class="label">Snapchat</span></a></li>
                            <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                            <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                            <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                            <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                        </ul>
                        <ul class="copyright">
                            <li>&copy;  All rights reserved.</li><li>Design: <a href="http://html5up.net"> Praxis Technical Team</a></li>
                        </ul>
            </footer>

        </div>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/jquery.scrolly.min.js"></script>
        <script src="../assets/js/jquery.dropotron.min.js"></script>
        <script src="../assets/js/jquery.scrollex.min.js"></script>
        <script src="../assets/js/skel.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="../assets/js/main.js"></script>


    </body>
</html>
