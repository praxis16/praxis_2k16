<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18-07-2016
 * Time: 14:51
 */
require(__DIR__."/../Classes/ApiMiddleware.php");
$api = new ApiMiddleware();

$request = array(
    'url' => 'events',
    'method' => 'GET',
    'body' => NULL
);
$responses = $api->initRequest($request);
//print_r($responses);
$event_data = $responses['data'];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Elements - Landed by HTML5 UP</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="../assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="../assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="../assets/css/ie8.css" /><![endif]-->
    <script language="javascript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <link href="../plugin/css/mb.balloon.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="../plugin/inc/jquery.mb.balloon.js"></script>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300);

        *{
            box-sizing: border-box;
        }

        body {
            font-family: 'Open Sans', sans-serif;
            color: #fff;
        }

        #wrapper{
            width: 80%;
            position: relative;
            margin: auto;
        }
        .opener {
            padding: 10px;
            text-align: center;
            cursor: default;
            max-width: 300px;
        }
        input.opener {
            padding: 10px;
            text-align: left;
            cursor: default;
            border: 1px solid #999;
            max-width: 300px;
        }

        .contentForBalloon{
            display: none;
            padding: 20px;
        }
    </style>
    <script>

        $(function () {


            jQuery.balloon.init();

        });

    </script>

</head>

<body class="landing">
<div id="page-wrapper">

    <header id="header">
        <h1 id="logo"><a href="../home.php">Praxis</a></h1>
        <nav id="nav">
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="events.php">Events</a></li>
                <li><a href="sponsors.php">Sponsors</a></li>
                <li><a href="schedule.php">Schedule</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
        </nav>
    </header>
    <section id="banner">
        <div class="content">
            <header>
                <h2>Events</h2>
            </header>
        </div>
        <a href="#main" class="goto-next scrolly">Next</a>
    </section>
    <div id="main" class="wrapper style1">

        <div class="container">
            <!--<header class="major">
                <h2>Events</h2>
            </header>-->
            <section>
                <div class="table-wrapper">
                    <table>
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Event Name</th>
                            <th>Cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($event_data as $data){
                            echo "<tr>";
                            echo "<td>".$data['category']."</td>";
                            echo "<td><span id='balloon4' class='opener' data-balloon = '{ajax} event-content.php?data=".$data['event_name']."''>".$data['event_name']."</span></td>";
                            echo "<td>".$data['cost']."</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>

                    </table>
                </div>
            </section>
        </div>
    </div>
    <footer id="footer">
        <ul class="icons">
            <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
            <li><a href="#" class="icon alt fa-snapchat"><span class="label">Snapchat</span></a></li>
            <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
            <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
            <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
            <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
            <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy;  All rights reserved.</li><li>Design: <a href="http://html5up.net"> Praxis Technical Team</a></li>
        </ul>
    </footer>
</div>
<!--<script src="../assets/js/jquery.min.js"></script>-->
<script src="../assets/js/jquery.scrolly.min.js"></script>
<script src="../assets/js/jquery.dropotron.min.js"></script>
<script src="../assets/js/jquery.scrollex.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
<script src="../assets/js/main.js"></script>
</body>
</html>
